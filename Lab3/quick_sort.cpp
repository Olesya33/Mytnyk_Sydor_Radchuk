#include <iostream>
#include <fstream> 
using namespace std;


void print(int arr[], int n) {
     
     for (int i = 0; i <n; i++) 
     {
         cout << arr[i] << "-";
     }
     cout << endl;
}


void quickSort(int arr[], int left, int right) { 
     
     int i = left, j = right;
     int tmp;
     int pivot = arr[(left + right) / 2];

     
     while (i <= j) {
           while (arr[i] < pivot)
                 i++;
           while (arr[j] > pivot)
                 j--;
           if (i <= j) {
           tmp = arr[i];
           arr[i] = arr[j];
           arr[j] = tmp;
           i++;
           j--;
           }
     }

     if (left < j)
     quickSort(arr, left, j); 
     if (i < right)
     quickSort(arr, i, right);

}

int main ()
{

    ifstream F1; 
    ofstream F2;
    
    F1.open("D:\\input.txt"); 
	int n;
	F1>>n; 
    cout<<"Array Size: "<<n<<endl;

    int i;
    int *arr=new int [n]; 
    
    if (F1) 
    {
       while (!F1.eof()) 
       {          	  
          for(i=0; !F1.eof(); i++)
          {
            F1>>arr[i];
            cout<< "Array["<< i+1 << "]:"<<arr[i]<<endl;  
                  
          }
          
       }     
    }
    F1.close(); 
    
    print(arr,n); 
    quickSort(arr, 0, n-1);
    
    
    F2.open("D:\\print.txt", ios::out);
    for (i=0; i<n; i++)
    {
        F2<<arr[i]<<"-";   
    }
    F2.close();
   
    print(arr,n); 
    return 0;

}
