#include <fstream>
#include <functional>
#include <climits>
#include <vector>
#include <queue>
#include <list>

using namespace std;

struct node {
	int vertex;
	int weight;
	node(int v, int w) : vertex(v), weight(w) { };
	node() { }
};

class CompareGreater {
	public:
		bool const operator()(node &nodeX, node &nodeY) {
			return (nodeX.weight > nodeY.weight) ;
		}
};

vector< list<node> > adj;
vector<int> weights;
priority_queue<node, vector<node>, CompareGreater> Q;

int nrVertices, nrEdges;

void readData();
void Dijkstra(node);
void writeData();

int main(int argc, char *argv[]) {

	readData();
	Dijkstra(node(1, 0));
	writeData();

	return 0;
}

void readData() {
	fstream in("input.txt", ios::in);

	int nodeX, nodeY, weight;

	in >> nrVertices >> nrEdges;

	adj.resize(nrVertices+1);
	weights.resize(nrVertices+1);

	for (int i = 1; i <= nrVertices; ++i) {
	weights.at(i) = INT_MAX;
	}

	for (int i = 1; i <= nrEdges; ++i) {
		in >> nodeX >> nodeY >> weight;
		adj[nodeX].push_back(node(nodeY, weight));
	}

	in.close();
}

void Dijkstra(node startNode) {
	node currentNode;

	weights[startNode.vertex] = 0;
	Q.push(startNode);

	while (!Q.empty()) {
		currentNode = Q.top();
		Q.pop();

		if (currentNode.weight <= weights[currentNode.vertex]) {
			for (list<node>::iterator it = adj[currentNode.vertex].begin(); it != adj[currentNode.vertex].end(); ++it) {
				if (weights[it->vertex] > weights[currentNode.vertex] + it->weight) {
					weights[it->vertex] = weights[currentNode.vertex] + it->weight;
					Q.push(node((it->vertex), weights[it->vertex]));
				}
			}
		}
	}
}

void writeData() {
	fstream out("dijkstra.txt", ios::out);

	weights.resize(nrVertices+1);
	for (vector<int>::iterator it = weights.begin()+1; it != weights.end(); ++it) {
		out << (*it) << " ";
	}

	out.close();
}
